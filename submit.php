<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Fontawesome -->
    <script src="https://use.fontawesome.com/563bb0680c.js"></script>

    <!-- Personal CSS -->
    <link rel="stylesheet" href="assets/CSS/the_style.css">

    <title>Brainster Labs</title>
</head>

<body>
    <!-- Navigation starts here -->
    <nav id="navigation" class="navbar navbar-expand-lg navbar-light bg-self-yellow">
        <a class="navbar-brand logo ml-5" href="index.php"><img src="assets/images/Brainster-Logo.jpg" alt="Brainster logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse colapse-self" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto justify-content-around">
                <li class="nav-item active  mr-md-5">
                    <a class="nav-link" href="https://marketpreneurs.brainster.co" target="blank">Академија за маркетинг <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active mr-md-5">
                    <a class="nav-link" href="http://codepreneurs.co/" target="blank">Академија за програмирање</a>
                </li>
                <li class="nav-item active mr-md-5">
                    <a class="nav-link" href="#" target="blank">Академија за data science</a>
                </li>
                <li class="nav-item active mr-md-5">
                    <a class="nav-link" href="https://design.brainster.co" target="blank">Академија за дизајн</a>
                </li>
                <li class="nav-item active ml-lg-5">
                    <a class="btn active-filter text-white" href="form.html">Вработи наш студент</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Navigation ends here -->
    <div class="container-fluid bg-self-yellow success-message">
        <div class="row">
            <div class="col">
                <?php
                $fullName = $_POST['fullName'];
                $companyName = $_POST['companyName'];
                $email = $_POST['email'];
                $phoneNumber = $_POST['phoneNumber'];
                $studentType = $_POST['studentType'];

                $conn = new mysqli('localhost', 'root', '', 'project-1');
                if ($conn->connect_error) {
                    die('Connection Failed : ' . $conn->connect_error);
                } else {
                    $stmt = $conn->prepare("insert into users(fullName, companyName, email, phoneNumber, studentType)
                         values(?, ?, ?, ?, ?)");
                    $stmt->bind_param("sssis", $fullName, $companyName, $email, $phoneNumber, $studentType);
                    $stmt->execute();
                    echo "<h1 class='text-center'>Регистрацијата е успешна</h1>";
                    $stmt->close();
                    $conn->close();
                }
                ?>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>